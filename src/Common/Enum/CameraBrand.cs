﻿namespace MG.CamCtrl
{
    public enum CameraBrand
    {
        /// <summary>
        /// 海康相机
        /// </summary>
        HIK,
        /// <summary>
        /// 大恒相机
        /// </summary>
        DaHeng,
        /// <summary>
        /// 巴斯勒相机 未添加
        /// </summary>
        Basler
    }
}


